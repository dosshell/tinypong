TinyPong

1. Update input invariant logic (AI etc)
2. Draw input invariant data
3. Wait for ultra sync
4. Read input
5. Do input dependent logic
6. Draw input dependent data
7. Blit the 2 draws
8. Wait for vsync

1-3 can be pipelined together with 6-8 (5 with special cases)