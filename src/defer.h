#pragma once

template <typename T>
class defer
{
public:
    explicit defer(T func) : func(func)
    {
    }
    ~defer()
    {
        if (func != nullptr)
          func();
    }
    defer(const defer& other) = delete;
    defer(const defer&& other) noexcept
    {
        this->func = other.func;
        other.func = nullptr;
    }
    defer& operator=(const defer& other) = delete;
    defer& operator=(defer&& other) noexcept
    {
        this->func = other.func;
        other.func = nullptr;
        return *this;
    }
    T func;
};
