#include "main.h"

#include "defer.h"

#include <resources.h>

#include <tinyfiber.h>
#include <tinycsocket.h>

#include <algorithm> // clamp
#include <cmath>
#include <thread>
#include <string>
#include <atomic>
#include <memory>

std::mt19937_64 g_rng;

void reset_match(GameState& gs)
{
    for (int i = 0; i < 2; ++i)
    {
        gs.player_pos[i] = ROOM_CENTER_Y;
        gs.player_prev_pos[i] = gs.player_pos[i];
    }
    gs.ball_x = ROOM_CENTER_X;
    gs.ball_y = ROOM_CENTER_Y;
    gs.ball_speed_y = 0;
    gs.ball_speed_x = -BALL_SPEED;
}

void init_match(GameState& gs)
{
    reset_match(gs);
    gs.score[0] = 0;
    gs.score[1] = 0;
}

int w2x(int64_t wc)
{
    int sx = int((wc * WINDOW_WIDTH + ROOM_WIDTH / 2) / ROOM_WIDTH);
    return sx;
}

int w2y(int64_t wc)
{
    int sy = int((wc * WINDOW_HEIGHT + ROOM_HEIGHT / 2) / ROOM_HEIGHT);
    return sy;
}

int64_t bounce_vert(int64_t rel_pos)
{
    static std::normal_distribution<> n{0, 2'000};
    int64_t r = int64_t(n(g_rng) * UNIT_LENGTH / UPDATE_PERIOD);
    return rel_pos * BALL_SPEED / PLAYER_HALF_HEIGHT + r;
}

void update_score_text(GameState& gs)
{
    std::string score_text = std::to_string(gs.score[0]) + "-" + std::to_string(gs.score[1]);
    gs.score_text = Text(score_text.c_str(), WINDOW_MIDDLE_X, 32, &sprite_font_roboto_mono);
}

void update_logic(GameState& gs)
{
    // Reset actions
    gs.action[0] = PlayerAction();
    gs.action[1] = PlayerAction();

    // AI input
    if (gs.ball_speed_x > 0)
    {
        // calculate intersection
        int64_t t = (BORDER_RIGHT - gs.ball_x) / gs.ball_speed_x;

        int64_t collision_y = gs.ball_y + gs.ball_speed_y * t;
        if (collision_y < 0)
        {
            collision_y = -collision_y;
        }
        if (collision_y > ROOM_HEIGHT)
        {
            collision_y = 2 * ROOM_HEIGHT - collision_y;
        }
        if (gs.player_pos[1] - PLAYER_SPEED > collision_y)
        {
            gs.action[1].up = true;
        }
        else if (gs.player_pos[1] + PLAYER_SPEED < collision_y)
        {
            gs.action[1].down = true;
        }
    }

    // Player input
    gs.action[0].up = tpx::input.is_down[tpx::KEY_UP];
    gs.action[0].down = tpx::input.is_down[tpx::KEY_DOWN];

    // Update player and ai positions
    for (int i = 0; i < 2; ++i)
    {
        gs.player_prev_pos[i] = gs.player_pos[i];
        if (gs.action[i].up)
            gs.player_pos[i] -= PLAYER_SPEED;
        if (gs.action[i].down)
            gs.player_pos[i] += PLAYER_SPEED;
        gs.player_pos[i] = std::clamp(gs.player_pos[i], POSITION_MIN, POSITION_MAX);
    }

    // Update ball position
    gs.ball_prev_x = gs.ball_x;
    gs.ball_prev_y = gs.ball_y;
    gs.ball_x += gs.ball_speed_x;
    gs.ball_y += gs.ball_speed_y;
    if (gs.ball_y < 0)
    {
        gs.ball_speed_y = -gs.ball_speed_y;
        gs.ball_y = -gs.ball_y;
    }
    if (gs.ball_y > ROOM_HEIGHT)
    {
        gs.ball_speed_y = -gs.ball_speed_y;
        gs.ball_y = 2 * ROOM_HEIGHT - gs.ball_y;
    }

    // Collisions
    for (int i = 0; i < NO_OF_BORDERS; ++i)
    {
        bool maybe_collision = false;
        if (gs.ball_speed_x < 0)
            maybe_collision = gs.ball_x < BORDERS[i] && gs.ball_prev_x >= BORDERS[i];
        else
            maybe_collision = gs.ball_x > BORDERS[i] && gs.ball_prev_x <= BORDERS[i];

        if (maybe_collision)
        {
            int64_t d = gs.ball_prev_x - gs.ball_x;
            int64_t dt = gs.ball_prev_x - BORDERS[i];
            int64_t k = dt / d;

            int64_t ball_collision_y = gs.ball_prev_y + (gs.ball_y - gs.ball_prev_y) * k;

            int64_t player_collision_pos = gs.player_prev_pos[i] + (gs.player_pos[i] - gs.player_prev_pos[i]) * k;
            if (ball_collision_y < player_collision_pos + PLAYER_HALF_HEIGHT && ball_collision_y > player_collision_pos - PLAYER_HALF_HEIGHT)
            {
                gs.ball_speed_x = -gs.ball_speed_x;
                gs.ball_x = 2 * BORDERS[i] - gs.ball_x;
                gs.ball_speed_y = bounce_vert(ball_collision_y - player_collision_pos);
            }
        }
    }

    // Score
    if (gs.ball_x < 0)
    {
        reset_match(gs);
        gs.score[1]++;
        update_score_text(gs);
    }
    else if (gs.ball_x > ROOM_WIDTH)
    {
        reset_match(gs);
        gs.score[0]++;
        update_score_text(gs);
    }
}

void draw(const GameState& gs)
{
    tpx::aquire_context(); // No await inside context ownership

    for (int i = 0; i < 2; ++i)
    {
        tpx::draw_line(
            w2x(BORDERS[i]), w2y(gs.player_pos[i] + PLAYER_HALF_HEIGHT), w2x(BORDERS[i]), w2y(gs.player_pos[i] - PLAYER_HALF_HEIGHT), WHITE, WHITE);
    }

    const int64_t BALL_HALF_WIDTH = (WINDOW_HEIGHT * BALL_HALF_HEIGHT) / WINDOW_WIDTH;
    tpx::draw_line(w2x(gs.ball_x - BALL_HALF_WIDTH),
                   w2y(gs.ball_y - BALL_HALF_HEIGHT),
                   w2x(gs.ball_x + BALL_HALF_WIDTH),
                   w2y(gs.ball_y + BALL_HALF_HEIGHT),
                   WHITE,
                   WHITE);
    tpx::draw_line(w2x(gs.ball_x + BALL_HALF_WIDTH),
                   w2y(gs.ball_y - BALL_HALF_HEIGHT),
                   w2x(gs.ball_x - BALL_HALF_WIDTH),
                   w2y(gs.ball_y + BALL_HALF_HEIGHT),
                   WHITE,
                   WHITE);

    gs.score_text.draw();

    tpx::next_frame();
    tpx::release_context();
}

RoomCommand singel_player()
{
    GameState gs{};
    init_match(gs);
    update_score_text(gs);
    int64_t last_logic_times = tpx::time.frame_timestamp;
    RoomCommand rc = RoomCommand::NONE;
    tpx::release_context(); // When we await we can swap thread
    while (rc == RoomCommand::NONE)
    {
        if (tpx::input.is_pressed[tpx::KEY_ESCAPE])
            rc = RoomCommand::MAIN_MENU;
        if (tpx::input.is_close_window)
            rc = RoomCommand::EXIT;

        int64_t steps = (tpx::time.frame_timestamp - last_logic_times) / UPDATE_PERIOD;
        for (int64_t i = 0; i < steps; ++i)
        {
            update_logic(gs);
            last_logic_times += UPDATE_PERIOD;
        }

        // Draw
        if (steps > 0)
            draw(gs);
    }
    tpx::aquire_context();
    return rc;
}

RoomCommand lan_menu()
{
    int selector = 0;
    const int NO_OF_SELECTIONS = 2;
    RoomCommand selections[NO_OF_SELECTIONS] = {RoomCommand::HOST_MP, RoomCommand::MAIN_MENU};
    Text text_deactive[NO_OF_SELECTIONS] = {Text("Host a New Game", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y - 32, &sprite_font_roboto_mono),
                                            Text("Back", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y + 32, &sprite_font_roboto_mono)};
    Text text_active[NO_OF_SELECTIONS] = {Text(">Host a New Game<", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y - 32, &sprite_font_roboto_mono),
                                          Text(">Back<", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y + 32, &sprite_font_roboto_mono)};

    RoomCommand rc = RoomCommand::NONE;
    while (rc == RoomCommand::NONE)
    {
        if (tpx::input.is_pressed[tpx::KEY_ESCAPE] || tpx::input.is_close_window)
            rc = RoomCommand::MAIN_MENU;

        if (tpx::input.is_pressed[tpx::KEY_ENTER])
            rc = selections[selector];

        if (tpx::input.is_pressed[tpx::KEY_DOWN])
            selector++;
        if (tpx::input.is_pressed[tpx::KEY_UP])
            selector--;
        selector = std::clamp(selector, 0, NO_OF_SELECTIONS - 1);

        // Draw
        for (int i = 0; i < NO_OF_SELECTIONS; ++i)
        {
            if (selector == i)
                text_active[i].draw();
            else
                text_deactive[i].draw();
        }
        tpx::next_frame();
    }
    return rc;
}

RoomCommand main_menu()
{
    static int selector = 0;
    const int NO_OF_SELECTIONS = 3;
    RoomCommand selections[NO_OF_SELECTIONS] = {RoomCommand::SINGEL_PLAYER, RoomCommand::LAN_MENU, RoomCommand::EXIT};
    Text text_deactive[NO_OF_SELECTIONS] = {Text("Singel Player", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y - 32, &sprite_font_roboto_mono),
                                            Text("LAN", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y, &sprite_font_roboto_mono),
                                            Text("Exit", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y + 32, &sprite_font_roboto_mono)};
    Text text_active[NO_OF_SELECTIONS] = {Text(">Singel Player<", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y - 32, &sprite_font_roboto_mono),
                                          Text(">LAN<", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y, &sprite_font_roboto_mono),
                                          Text(">Exit<", WINDOW_MIDDLE_X, WINDOW_MIDDLE_Y + 32, &sprite_font_roboto_mono)};
    RoomCommand rc = RoomCommand::NONE;
    while (rc == RoomCommand::NONE)
    {
        if (tpx::input.is_pressed[tpx::KEY_ESCAPE] || tpx::input.is_close_window)
            rc = RoomCommand::EXIT;

        if (tpx::input.is_pressed[tpx::KEY_ENTER])
            rc = selections[selector];

        if (tpx::input.is_pressed[tpx::KEY_DOWN])
            selector++;
        if (tpx::input.is_pressed[tpx::KEY_UP])
            selector--;
        selector = std::clamp(selector, 0, NO_OF_SELECTIONS - 1);

        // Draw
        for (int i = 0; i < NO_OF_SELECTIONS; ++i)
        {
            if (selector == i)
                text_active[i].draw();
            else
                text_deactive[i].draw();
        }

        tpx::next_frame();
    }
    return rc;
}

RoomCommand host_mp()
{
#ifdef MULTIPLAYER
    tcs_socket soc = TCS_NULLSOCKET;
    tcs_create(&soc, TCS_AF_INET, TCS_SOCK_DGRAM, TCS_IPPROTO_UDP);
    tcs_setsockopt(soc, TCS_IPPROTO_IP, TCS_IP_MULTICAST_LOOP, 0, 1);
    uint8_t ttl = 1;
    tcs_setsockopt(soc, TCS_IPPROTO, TCS_IP_MULTICAST_TTL, &ttl, sizeof(ttl));
    /*
    struct in_addr interface_addr;
    SIOCGIFCONF
setsockopt (socket, IPPROTO_IP, IP_MULTICAST_IF, &interface_addr, sizeof(interface_addr));
    */


#endif
    return RoomCommand::MAIN_MENU;
}

int main(int argc, const char argv[])
{
    std::atomic<bool> is_inited = false;

    std::thread gui_thread([&]() {
        tpx::open_window("TinyPong", WINDOW_WIDTH, WINDOW_HEIGHT, false, true, MAX_FPS);
        tpx::release_context();
        is_inited = true;
        tpx::start_gui_loop();
        tpx::close_window();
    });

    while (!is_inited)
        std::this_thread::yield();

    tfb_init();
    tpx::aquire_context();
    resources_load_all();

    bool should_exit = false;
    RoomCommand rc = RoomCommand::MAIN_MENU;
    while (!should_exit)
    {
        switch (rc)
        {
            case RoomCommand::MAIN_MENU:
                rc = main_menu();
                break;
            case RoomCommand::SINGEL_PLAYER:
                rc = singel_player();
                break;
            case RoomCommand::LAN_MENU:
                rc = lan_menu();
                break;
            case RoomCommand::EXIT:
                should_exit = true;
                break;
            default:
                break;
        }
    }

    resources_unload_all();
    tfb_free();

    tpx::stop_gui_loop();
    gui_thread.join();

    return 0;
}
