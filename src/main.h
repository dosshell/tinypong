#pragma once

#include <stdint.h>
#include <tinypixels.h>

#include <random>

static const int64_t MAX_FPS = 120;
static const int64_t UPDATE_PERIOD = (2'000'000 + MAX_FPS) / (2 * MAX_FPS); // in us
static const int64_t UNIT_LENGTH = UPDATE_PERIOD;

static const int64_t ROOM_WIDTH = 1'000 * UNIT_LENGTH;
static const int64_t ROOM_HEIGHT = 1'000 * UNIT_LENGTH;
static const int64_t ROOM_CENTER_X = ROOM_WIDTH / 2;
static const int64_t ROOM_CENTER_Y = ROOM_HEIGHT / 2;

static const int64_t PLAYER_HALF_HEIGHT = 50 * UNIT_LENGTH;
static const int64_t PLAYER_SPEED = 50'000 * UNIT_LENGTH / UPDATE_PERIOD;

static const int64_t POSITION_MAX = ROOM_HEIGHT - PLAYER_HALF_HEIGHT;
static const int64_t POSITION_MIN = PLAYER_HALF_HEIGHT;

static const int64_t BORDER_LEFT = ROOM_WIDTH / 10;
static const int64_t BORDER_RIGHT = ROOM_WIDTH - BORDER_LEFT;
static const int NO_OF_BORDERS = 2;
static const int64_t BORDERS[NO_OF_BORDERS] = {BORDER_LEFT, BORDER_RIGHT};

static const int64_t BALL_HALF_HEIGHT = 10 * UNIT_LENGTH;
static const int64_t BALL_SPEED = 50'000 * UNIT_LENGTH / UPDATE_PERIOD;

static const int64_t WINDOW_WIDTH = 800;
static const int64_t WINDOW_HEIGHT = 600;
static const int64_t WINDOW_MIDDLE_X = WINDOW_WIDTH / 2;
static const int64_t WINDOW_MIDDLE_Y = WINDOW_HEIGHT / 2;

static const tpx::Color WHITE = 0xFFFFFFFF;

extern std::mt19937_64 g_rng;

class Text
{
public:
    Text() : m_font(nullptr)
    {
    }
    Text(const char text[], int x, int y, const tpx::SpriteResource* font, bool center = true) : m_font(font)
    {
        const int L = (int)std::char_traits<char>::length(text);
        const int char_width = m_font->width;
        const int text_width = L * char_width;

        int justified_x = center ? x - text_width / 2 : x;

        for (int i = 0; i < L; ++i)
        {
            tpx::SpriteInstance instance;
            instance.x = justified_x + i * char_width;
            instance.y = y;
            instance.subimage = text[i];
            m_spr_instances.push_back(instance);
        }
    }
    void draw() const
    {
        tpx::draw_sprites(m_font, m_spr_instances.data(), (int)m_spr_instances.size());
    }

protected:
    std::vector<tpx::SpriteInstance> m_spr_instances;
    const tpx::SpriteResource* m_font;
};

struct PlayerAction
{
    bool up;
    bool down;
};

struct GameState
{
    int64_t player_pos[2];
    int64_t player_prev_pos[2];
    int64_t ball_x;
    int64_t ball_y;
    int64_t ball_prev_x;
    int64_t ball_prev_y;
    int64_t ball_speed_y;
    int64_t ball_speed_x;

    Text score_text;
    int score[2];
    PlayerAction action[2];
};

enum class RoomCommand
{
    NONE,
    MAIN_MENU,
    SINGEL_PLAYER,
    HOST_MP,
    LAN_MENU,
    EXIT,
};
